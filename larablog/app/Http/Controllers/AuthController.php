<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }
    
    public function welcoming(Request $request) {
        // dd($request->all());
        $fname = $request["fname"];
        $lname = $request["lname"];
        return view('welcoming', compact('fname', 'lname'));
    }
}
