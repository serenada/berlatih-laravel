<!DOCTYPE html>
<html>
    <head>
        <title>SanberBook</title>
    </head>

    <body>
        <!-- Bagian awal -->
        <div>
            <h3> Buat Account Baru </h3> 
            <h4> Sign Up Form </h4>
        </div>
        
         <!-- Nama -->
            <form action="{{route('welcoming')}}" method="POST">
            {{csref_field()}}
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
                <label for="fname">First Name:</label>
                <br><br>
                <input type="text" id="fname">
                <br><br>
                <label for="lname">Last Name:</label>
                <br><br>
                <input type="text" id="lname">
                <br><br>
            
        <!-- Gender -->
            <div>
                <label>Gender:</label> <br><br>
                <input type="radio" name="gender" value="0"> Male <br>
                <input type="radio" name="gender" value="1"> Female <br>
                <input type="radio" name="gender" value="2"> Other <br><br>
            </div>

        <!-- Nationality -->
            <div>
                <label>Nationality:</label> <br><br>
                <select>
                    <option value="0"> Indonesian </option>
                    <option value="1"> American </option>
                    <option value="2"> Japanese </option>
                    <option value="3"> Korean </option>
                </select>  <br><br>
            </div>

        <!-- Bahasa -->
            <div>
                <label>Language Spoken:</label> <br><br>
                <input type="checkbox" name="bahasa" value="0"> Bahasa Indonesia <br>
                <input type="checkbox" name="bahasa" value="1"> English <br>
                <input type="checkbox" name="bahasa" value="2"> Other <br><br>
            </div>

        <!-- Bio -->
            <div>
                <label for="bio">Bio:</label> <br><br>
                <textarea cols="35" rows="10"> </textarea>
                <br>
            </div>

        <!-- Submit -->
            <div>
                <input type="submit" value="Sign Up" formaction="welcoming">
            </div>

            </form>
    </body>
</html>