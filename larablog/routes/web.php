<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/index', function () {
    return view('index');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/welcoming', function () {
    return view('welcoming');
});
*/

/*
Route::view('index', 'index');
Route::view('register', 'register');
Route::view('welcoming', 'welcoming');
*/

/* Route::get ('/welcome', function () {
    $nama = "Serenada";
    return $nama;
});
*/

Route::get('/index', 'HomeController@home');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/welcoming', 'AuthController@welcome')->name('welcoming');
