<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{

    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        $request->validate([
            'title' => 'required|unique:pertanyaan',
            'body' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);
        return redirect('/pertanyaan/create');
    }

    public function index() {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id) {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function edit($id) {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }
}
